/**
 * Created by chaika on 10.04.16.
 */
var Variant = window.VARIANT;
var Tasks = require('./tasks/tasks');
var configuration = require('./utils/configuration');
var ConfigsTemplate = Tasks.BaseTasks;

var Environment = require('./utils/classes/Environment');


$(function(){
    var $population = $("#population");
    var $iterations = $("#iterations");
    var $statistics = $("#statistics");
    var $probability = $("#probability");
    var $encoding = $("#encoding");
    var $selection = $("#selection");
    var $variables = $("#variables");
    var $results = $("#results");
    var $generalResults = $("#general-result");

    function getCurrentConfig() {
        var config = configuration.cloneConfig(ConfigsTemplate.Common);
        var population = parseInt($population.val());
        var iteration = parseInt($iterations.val());
        var statistics = parseInt($statistics.val());
        var variables = parseInt($variables.val());
        var probability = parseFloat($probability.val());

        var encoding = $encoding.val();
        var selection = $selection.val();


        config.PopulationSize = population;
        config.MaxIterations = iteration;
        config.StatsSteps = statistics;

        configuration.append(config, ConfigsTemplate.Variants.Encoding[encoding]);
        configuration.append(config, ConfigsTemplate.Variants.Selection[selection]);

        config.PhenotypeGenerator = config.PhenotypeGenerator(variables);
        config.FitnessFunction = Tasks.Functions[Variant];
        config.Mutation = config.Mutation(probability);

        return config;
    }

    function updateStatistics(stats) {
        var header = '<div class="table-responsive">' +
            '<table class="table table-hover table-bordered table-striped table-condensed">' +
            '<thead>' +
            '<tr>' +
            '<th>Ітерація</th>' +
            '<th>Пристосованість найкращого</th>' +
            '<th>Середня пристосованість</th>' +
            '<th>Генотип найкращого</th>' +
            '<th>Фенотип найкращого</th>' +
            '</tr>' +
            '</thead><tbody>';
        var footer = "</tbody></table></div>";

        function formatPhenotype(p) {
            if(Array.isArray(p)) {
                return "[" + p.map(function(v){
                        return v.toFixed(2);
                    }).join(",") + "]"
            }
            return p;
        }

        function oneRow(row) {
            return '<tr>' +
                '<td>'+row.iterations+'</td>' +
                '<td>'+row.f_best+'</td>' +
                '<td>'+row.f_average+'</td>' +
                '<td>'+formatPhenotype(row.best_genotype)+'</td>' +
                '<td>'+formatPhenotype(row.best_phenotype)+'</td>' +
                '</tr>';
        }

        var inner = stats.map(oneRow).join("\n");
        var html = header + inner + footer;

        $results.html(html);
        //console.log(stats);
    }

    function runSimulation(callback) {
        $generalResults.html("");
        var config = getCurrentConfig();

        var env = new Environment(config);
        env.runSimulationInteractive(updateStatistics, function(results){
            var html = "";
            if(results.converged) {
                html = '<div class="alert alert-success" role="alert">Збіжність досягнуто</div>';
            } else {
                html = '<div class="alert alert-danger" role="alert">Збіжність не досягнуто</div>';
            }

            $generalResults.html(html);
            callback();
        });
    }


    var RUN_BUTTON = $(".run");
    RUN_BUTTON.click(function(){
        RUN_BUTTON.prop("disabled", true);

        runSimulation(function(){
            RUN_BUTTON.prop("disabled", false);
        });
    });
});



