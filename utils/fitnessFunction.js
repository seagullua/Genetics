/**
 * Created by chaika on 29.03.16.
 */

exports.createFitnessCalculator = function(fn, transform) {
    var Cache = {};

    var cacheKey = transform.cacheKey;
    //var encode = transform.encode;
    var decode = transform.decode;

    return function(item) {
        var key = cacheKey(item);
        if(key && key in Cache) {
            return Cache[key];
        }

        var decoded = decode(item);
        var value = fn(decoded);

        if(key) {
            Cache[key] = value;
        }
        return value;
    }
};