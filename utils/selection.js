/**
 * Created by chaika on 29.03.16.
 */
var RWS = require('./selection/RWS');
var SUS = require('./selection/SUS');

exports.RouletteWheelSelectionMin =
    function() {
        return RWS.createRouletteWheelSelection(RWS.Mode.Minimise);
    };
exports.RouletteWheelSelectionMax = function() {
    return RWS.createRouletteWheelSelection(RWS.Mode.Maximise);
};

exports.SUSSelectionMin =
    function() {
        return SUS.createSUSSelection(RWS.Mode.Minimise);
    };
exports.SUSSelectionMax = function() {
    return SUS.createSUSSelection(RWS.Mode.Maximise);
};
