/**
 * Created by chaika on 29.03.16.
 */

function Individual(genotype) {
    this.genotype = genotype;
    this.fitness = null;
}
Individual.prototype.clone = function(env) {
    var ind = new Individual(env.cloneGenotype(this.genotype));
    ind.fitness = this.fitness;
    return ind;
};
Individual.prototype.setMutatedGenotype = function(val) {
    this.genotype = val;
    this.fitness = null;
};

module.exports = Individual;