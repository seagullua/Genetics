/**
 * Created by chaika on 29.03.16.
 */
function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}

function generateRandomVector(min, max, length) {
    var result = [];
    for(var i=length-1; i>=0; --i) {
        result[i] = getRandom(min, max);
    }
    return result;
}

exports.RandomVectorGenerator = function(min, max) {
    return function(length) {
        return generateRandomVector(min, max, length);
    }
};

exports.randomVector = generateRandomVector;
exports.randomNumber = getRandom;