/**
 * Created by chaika on 30.03.16.
 */
var Environment = require('../utils/classes/Environment');
var fitnessFunction = require('../utils/fitnessFunction');
var functions = require('../utils/functions');
var transforms = require('../utils/transforms');
var random = require('../utils/random');
var selection = require('../utils/selection');
var mutation = require('../utils/mutation');
var statistics = require('../utils/statistics');

var F_MIN = -5.12;
var F_MAX = 5.12;
var BITS_PER_VARIABLE = 10;

var MUTATION_LINEAR_R = 0.1024;
var MUTATION_GAUSS_V = 0.6;


var Tasks = {
    Common: {
        PhenotypeGenerator: random.RealPhenotypeGeneratorAnyLength(F_MIN, F_MAX),
        OptimalPhenotypeGenerator: functions.getZeroPhenotypeGenerator(),
        BasePopulationSize: 100,
        MaxIterations: 100000,
        StatsSteps: 10000
    },
    Variants: {
        Encoding: {
            EncodingBinaryGray: {
                Transform: transforms.Vector_RealToGrayTransform(
                    BITS_PER_VARIABLE, F_MIN, F_MAX),
                Mutation: mutation.BinaryMutation(),
                StartPMaxSearch: 0.00001,
                HemmingDistance: statistics.HammingDistance.stringsDistance
            },
            EncodingBinaryBinary: {
                Transform: transforms.Vector_RealToBinaryTransform(
                    BITS_PER_VARIABLE, F_MIN, F_MAX),
                Mutation: mutation.BinaryMutation(),
                StartPMaxSearch: 0.00001,
                HemmingDistance: statistics.HammingDistance.stringsDistance
            },
            EncodingRealLinear: {
                Transform: transforms.Vector_NoneTransform(),
                Mutation: mutation.LinearRealMutation(MUTATION_LINEAR_R),
                StartPMaxSearch: 0.05,
                HemmingDistance: statistics.HammingDistance.arrDistance
            },
            EncodingRealGauss: {
                Transform: transforms.Vector_NoneTransform(),
                Mutation: mutation.GaussRealMutation(MUTATION_GAUSS_V),
                StartPMaxSearch: 0.05,
                HemmingDistance: statistics.HammingDistance.arrDistance
            }
        },
        Selection: {
            SelectionSUS: {
                Selection: selection.SUSSelectionMin()
            },
            SelectionRWS: {
                Selection: selection.RouletteWheelSelectionMin()
            }
        }
    }
};

exports.getProbabilitiesToCheck = function(p_max) {
    return [
        {
            id: "0.1_P_max",
            title: "0.1*P_max",
            prob: 0.1*p_max
        },
        {
            id: "0.2_P_max",
            title: "0.2*P_max",
            prob: 0.2*p_max
        },
        {
            id: "0.8_P_max",
            title: "0.8*P_max",
            prob: 0.8*p_max
        },
        {
            id: "0.9_P_max",
            title: "0.9_P_max",
            prob: 0.9*p_max
        },
        {
            id: "P_max",
            title: "P_max",
            prob: p_max
        },

        {
            id: "1.1_P_max",
            title: "1.1_P_max",
            prob: 1.1*p_max
        },
        {
            id: "1.2_P_max",
            title: "1.2_P_max",
            prob: 1.2*p_max
        }

        //{
        //    id: "10_P_max",
        //    title: "10*P_max",
        //    prob: 10*p_max
        //},

        //{
        //    id: "5_P_max",
        //    title: "5*P_max",
        //    prob: 5*p_max
        //},




    ]
};

exports.BaseTasks = Tasks;
exports.FunctionVariables = [1,2,3,5,10,20,50];
exports.Functions = {
    Variant1: functions.Variant1,
    Variant2: functions.Variant2,
    Variant3: functions.Variant3,
    Variant4: functions.Variant4,
    Variant5: functions.Variant5,
    Variant6: functions.Variant6,
    Variant7: functions.Variant7
};