/**
 * Created by chaika on 29.03.16.
 */
var random = require('../random');
var RWS = require('./RWS');

function generateNewPopulation(mode, environment, population) {
    var total_fitness = environment.getPopulationFitness(population);
    var N = population.N;
    var pointers_distance = total_fitness / N;
    var start = random.randomNumber(0, pointers_distance);

    var points = [];
    for(var i=0; i<N; ++i) {
        points.push((start + i*pointers_distance) / total_fitness);
    }
    return RWS.performRWSSelectionOnPoints(mode, environment, population, points);
}

function createSUSSelection(mode) {
    return function(environment) {
        return {
            generateNewPopulation: function(population) {
                return generateNewPopulation(mode, environment, population);
            }
        }
    }
}

exports.createSUSSelection = createSUSSelection;

