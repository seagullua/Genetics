/**
 * Created by chaika on 29.03.16.
 */
function Population(individuals) {
    this.inidividuals = individuals;
    this.total_fitness = null;
    this.best_fitness = null;

    this.N = individuals.length;
}

Population.cloneIndividuals = function(individuals, env) {
    return individuals.map(function(ind){
        return ind.clone(env);
    });
};

module.exports = Population;