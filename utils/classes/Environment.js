/**
 * Created by chaika on 29.03.16.
 */
var fitnessFunction = require('../fitnessFunction');
var random = require('../random');
var Population = require('./Population');
var Individual = require('./Individual');

function Environment(params) {
    this._Transform = params.Transform;
    this._FitnessFunction = params.FitnessFunction;
    this._Selection = params.Selection;
    this._PhenotypeGenerator = params.PhenotypeGenerator;
    this._PopulationSize = params.PopulationSize;
    this._Mutation = params.Mutation;
    this._MaxIterations = params.MaxIterations;
    //Show stats each time of iterations
    this._StatsSteps = params.StatsSteps;

    //Stats
    this._iterations = 0;
    this._average_fitness = 0;
    this._best_fitness = 0;
    this._average_fitness_history = [];
    this._fitness_calculation = 0;

    this._FitnessCalculator = fitnessFunction.createFitnessCalculator(this._FitnessFunction, this._Transform);

    this._SelectionCalculator = this._Selection(this);

}

Environment.prototype.getIndividualFitness = function(individual) {
    this._fitness_calculation++;
    if(individual.fitness === null) {
        individual.fitness = this._FitnessCalculator(individual.genotype);
    }
    return individual.fitness;
};

Environment.prototype.getIndividualPhenotype = function(ind) {
    return this._Transform.decode(ind.genotype);
};

Environment.prototype.getPopulationFitness = function(population) {
    var __best_fitness = 10000000000;
    var best = null;

    if(population.total_fitness === null) {
        var total = 0;
        var individuals = population.inidividuals;
        for(var i=0; i<individuals.length; ++i) {
            var fitness = this.getIndividualFitness(individuals[i]);
            total += fitness;

            if(fitness < __best_fitness) {
                __best_fitness = fitness;
                best = individuals[i];
            }
        }
        population.best_fitness = __best_fitness;
        population.total_fitness = total;
        population.best = best;
    }
    return population.total_fitness;
};

Environment.prototype.mutatePopulation = function(population) {
    var self = this;
    var indivs = population.inidividuals;
    for(var i=0; i<indivs.length; ++i) {
        var ind = indivs[i];
        var genotype = ind.genotype;
        var res = self._Mutation(genotype);

        if(res.mutated) {
            ind.setMutatedGenotype(res.result);
        }
    }
    return population;
};

Environment.prototype.generateNewPopulation = function(old_population) {
    var result = this._SelectionCalculator.generateNewPopulation(old_population);

    if(result.N != this._PopulationSize) {
        throw new Error("Wrong population size " + result.N);
    }
    return result;
};

Environment.prototype.seedPopulation = function() {
    var phenotypes = random.generatePhenotype(this._PhenotypeGenerator, this._PopulationSize);

    var self = this;
    var individuals = phenotypes.map(function(phen){
        var genotype = self._Transform.encode(phen);
        return new Individual(genotype);
    });

    return new Population(individuals);
};

Environment.prototype.hasConverged = function() {
    var average_fitness = this._average_fitness;
    var best_fitness = this._best_fitness;
    function isSame(a, b) {
        return Math.abs(a-b) < 0.00001;
    }

    if(Math.abs(best_fitness - average_fitness) < 0.01) {

        var history = this._average_fitness_history;
        var CHECK_LAST = 5;
        if(history.length >= CHECK_LAST) {
            var same = true;
            for(var i = history.length - 1; i >= history.length - CHECK_LAST; --i) {
                if(!isSame(average_fitness, history[i])) {
                    same = false;
                    break;
                }
            }
            return same;
        }
    }
    return false;
};

Environment.prototype.hasFinished = function() {
    return this._iterations >= this._MaxIterations || this.hasConverged();
};
Environment.prototype.cloneGenotype = function(genotype) {
    return this._Transform.cloneGenotype(genotype);
};

Environment.prototype.readPopulationStats = function(population) {
    var total_fitness = this.getPopulationFitness(population);
    var average_fitness = total_fitness / population.N;

    var best_fitness = population.best_fitness;
    this._average_fitness = average_fitness;
    this._average_fitness_history.push(average_fitness);
    this._best_fitness = best_fitness;
    this._best_indiv = population.best;
};

function formatNumber(number) {
    return Math.round(number * 10000) / 10000;
}

Environment.prototype.showStats = function() {
    console.log("Iter: ", this._iterations, "Best: ", formatNumber(this._best_fitness), "Average: ", formatNumber(this._average_fitness));
};

Environment.prototype.showStatsInteractive = function(statistics_callback, callback) {
    var row = {
        iterations: this._iterations,
        f_best: formatNumber(this._best_fitness),
        f_average: formatNumber(this._average_fitness),
        best_genotype: this._best_indiv.genotype,
        best_phenotype: this._Transform.decode(this._best_indiv.genotype)
    };

    this.stats.push(row);

    statistics_callback(this.stats);
    setTimeout(callback, 200);
};

Environment.prototype.runSimulation = function() {
    var population = this.seedPopulation();

    this.readPopulationStats(population);
    this.showStats();

    while(!this.hasFinished()) {
        population = this.generateNewPopulation(population);
        this.mutatePopulation(population);

        this.readPopulationStats(population);
        this._iterations++;

        if(this._iterations % this._StatsSteps == 0) {
            this.showStats();
        }
    }

    this.showStats();
    var ind = population.best;

    return {
        population: population,
        average: this._average_fitness,
        best: this._best_fitness,
        iterations: this._iterations,
        converged: this.hasConverged(),
        ind: ind,
        phenotype: this.getIndividualPhenotype(ind),
        fitnessCalled: this._fitness_calculation
    }
};

Environment.prototype.runSimulationInteractive = function(statistics_callback, callback) {
    var population = this.seedPopulation();
    this.stats = [];

    this.readPopulationStats(population);

    var self = this;
    function iteration(){
        while(!self.hasFinished()) {
            population = self.generateNewPopulation(population);
            self.mutatePopulation(population);

            self.readPopulationStats(population);
            self._iterations++;

            if(self._iterations % self._StatsSteps == 0) {
                break;

            }
        }

        if(!self.hasFinished()) {
            self.showStatsInteractive(statistics_callback, iteration);
        } else {
            self.showStatsInteractive(statistics_callback, function(){
                var ind = population.best;

                callback({
                    population: population,
                    average: self._average_fitness,
                    best: self._best_fitness,
                    iterations: self._iterations,
                    converged: self.hasConverged(),
                    ind: ind,
                    phenotype: self.getIndividualPhenotype(ind),
                    fitnessCalled: self._fitness_calculation
                });
            });
        }

    }

    this.showStatsInteractive(statistics_callback, iteration);
};

module.exports = Environment;
