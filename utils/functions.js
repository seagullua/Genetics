/**
 * Created by chaika on 29.03.16.
 */
exports.Variant1 = require('./functions/Variant1');
exports.Variant2 = require('./functions/Variant2');
exports.Variant3 = require('./functions/Variant3');
exports.Variant4 = require('./functions/Variant4');
exports.Variant5 = require('./functions/Variant5');
exports.Variant6 = require('./functions/Variant6');
exports.Variant7 = require('./functions/Variant7');

exports.getZeroPhenotypeGenerator = function() {
    return function(varibles) {
        var result = [];
        for(var i=0;i<varibles; ++i) {
            result.push(0);
        }
        return result;
    }
};