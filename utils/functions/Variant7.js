/**
 * Created by chaika on 29.03.16.
 */
//Варіант 2.
//Паралельна осям гіпереліпсоїдна функція, обтяжена сферична функція, зважена сферична модель (Axis parallel hyper-ellipsoid function, Weighted sphere model, extended homogeneous quadratic function):
var a = 20;
var b = 0.2;
var c = 2 * Math.PI;
var exp_1 = Math.exp(1);

module.exports = function(x_arr) {
    var size = x_arr.length;
    var sum1 = 0;
    var sum2 = 0;
    var n_div = 1.0 / size;

    for(var i=0; i<size; ++i) {
        var x = x_arr[i];
        sum1 += x * x;
        sum2 += Math.cos(c * x);
    }

    sum1 = Math.sqrt(n_div * sum1);
    sum2 = sum2 * n_div;

    return -a * Math.exp(-b * sum1) - Math.exp(sum2) + a + exp_1;
};