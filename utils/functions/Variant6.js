/**
 * Created by chaika on 29.03.16.
 */

module.exports = function(x_arr) {
    var size = x_arr.length;
    var result = 0;
    var product = 1;

    for(var i=0; i<size; ++i) {
        var x = x_arr[i];
        result += x*x;
        product *= Math.cos(x / Math.sqrt(i+1))
    }
    result /= 4000;
    return result - product + 1;
};