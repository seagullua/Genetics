/**
 * Created by chaika on 29.03.16.
 */
function chunkB(arr, n) {
    var i, j, temparray = [], chunk = n;
    for (i = 0, j = arr.length; i < j; i += chunk) {
        temparray.push(arr.slice(i, i + chunk));
    }
    return temparray
}

exports.VectorBinaryTransform = function(bits_per_variable, variable_transform) {
    var transform = variable_transform(bits_per_variable);

    return {
        encode: function(arr) {
            return arr.map(transform.encode).join("");
        },
        decode: function(str) {
            var chunks = chunkB(str, bits_per_variable);
            return chunks.map(transform.decode);
        },
        cacheKey: function(str) {
            return str;
        },
        cloneGenotype: function(genotype) {
            return genotype
        }
    };
};