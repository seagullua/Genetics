/**
 * Created by chaika on 29.03.16.
 */
var binary = require('./transforms/binary');
var combined = require('./transforms/combined');
var gray = require('./transforms/gray');
var realToInteger = require('./transforms/realToInteger');

var vector_binaryTransform = require('./vectorTransforms/binaryTransform');
var vector_noneTransform = require('./vectorTransforms/noneTransform');

var RealToGrayTransform = function(min, max) {
    return combined.CombinedTransform(
        realToInteger.RealToIntegerTransform(min, max),
        gray.GrayTransform()
    )
};

var RealToBinaryTransform = function(min, max) {
    return combined.CombinedTransform(
        realToInteger.RealToIntegerTransform(min, max),
        binary.BinaryTransform()
    )
};

var Vector_NoneTransform = function() {
    return vector_noneTransform.VectorNoneTransform();
};

var Vector_RealToGrayTransform = function(bits_per_variable, min, max) {
    return vector_binaryTransform.VectorBinaryTransform(
        bits_per_variable,
        RealToGrayTransform(min, max)
    );
};

var Vector_RealToBinaryTransform = function(bits_per_variable, min, max) {
    return vector_binaryTransform.VectorBinaryTransform(
        bits_per_variable,
        RealToBinaryTransform(min, max)
    );
};


exports.Vector_NoneTransform = Vector_NoneTransform;
exports.Vector_RealToGrayTransform = Vector_RealToGrayTransform;
exports.Vector_RealToBinaryTransform = Vector_RealToBinaryTransform;