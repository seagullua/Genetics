/**
 * Created by chaika on 30.03.16.
 */
var Tasks = require('../tasks/tasks');
var configuration = require('./configuration');
var OUTPUT_DIR = 'output/pmax';
var path = require('path');
var fse = require('fs-extra');
var Environment = require('./classes/Environment');
var CONVERGES_CHECKS = 2;

function readJSON(file) {
    if(fse.existsSync(file)) {
        return fse.readJsonSync(file);
    }
    return null;
}

function writeJSON(file, json) {
    fse.writeJsonSync(file, json);
}

function isConfigConverges(config, probability, title) {
    console.log("Check converges", title, probability);
    config = configuration.cloneConfigForProbability(config, probability);

    var converges = true;

    for(var i=0; i<CONVERGES_CHECKS; ++i) {
        var env = new Environment(config);
        var result = env.runSimulation();

        if(!result.converged) {
            console.log("Not converged ", i, title, probability);
            converges = false;
            break;
        } else {
            console.log("Converged ", i, title, probability);
        }
    }
    return converges;
}

function getNextProbabilityToCheck(cache, config) {
    var final_prob = 0;
    var final_result = false;

    var max_converges = null;
    var min_not_converges = null;

    cache.forEach(function(item){
        var converges = item.converged;
        var prob = item.prob;

        if(converges && (prob > max_converges || max_converges === null)) {
            max_converges = prob;
        }

        if(!converges && (prob < min_not_converges || min_not_converges === null)) {
            min_not_converges = prob;
        }
    });

    if(min_not_converges === null && max_converges === null) {
        //Starting point
        final_prob = config.StartPMaxSearch || 0.000001;
    } else if(min_not_converges === null) {
        final_prob = max_converges * 3;
    } else if(max_converges === null) {
        final_prob = min_not_converges / 3;
    } else {
        var diff = min_not_converges / max_converges;
        final_prob = (min_not_converges + max_converges) / 2;
        if(diff < 1.1 && diff > 0.9) {
            final_result = true;
        }
    }

    return {
        finalResult: final_result,
        prob: final_prob
    }
}

function findPMaxForConfig(dir, config, title) {
    dir = path.join(OUTPUT_DIR, dir);
    var file_name = path.basename(dir);
    var dir_name = path.dirname(dir);

    var RESULT_FILE = path.join(dir_name, file_name + "_RESULT.json");
    var CACHE_FILE = path.join(dir_name, file_name + "_CACHE.json");

    fse.ensureDirSync(dir_name);

    var result = readJSON(RESULT_FILE);
    if(result !== null) {
        return result.prob;
    }

    var cache = readJSON(CACHE_FILE);
    if(!cache) {
        cache = [];
    }

    while(true) {
        var find_next = getNextProbabilityToCheck(cache, config);
        if(find_next.finalResult) {
            writeJSON(RESULT_FILE, {prob: find_next.prob});
            return find_next.prob;
        } else {
            var conv = isConfigConverges(config, find_next.prob, title);
            cache.push({
                converged: conv,
                prob: find_next.prob
            });
            writeJSON(CACHE_FILE, cache);
        }
    }
}

function findPMaxAllConfigs(fn_title) {
    var configs = configuration.generateConfigurations(Tasks.BaseTasks);
    var fn = Tasks.Functions[fn_title];

    if(!fn) {
        throw new Error("Wrong fn "+fn_title);
    }

    var dimensions = Tasks.FunctionVariables;

    configs = configuration.createFunctionAndDimensionsConfigs(configs, fn_title, fn, dimensions);

    var results = {};
    Object.keys(configs).forEach(function(id){
        console.log("PMax ", id);

        results[id] = findPMaxForConfig(id, configs[id], id);
        console.log("PMax= " + results[id], id);
    });
    return results;
}

exports.findPMaxAllConfigs = findPMaxAllConfigs;