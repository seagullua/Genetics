/**
 * Created by chaika on 29.03.16.
 */
//Варіант 1.
//Сферична функція, сферична модель, тестова функція Де Йонга 1 (Sphere function, Sphere model, De Jong’s test function 1):

module.exports = function(x_arr) {
    var result = 0;
    var size = x_arr.length;
    for(var i=0; i<size; ++i) {
        var x = x_arr[i];
        result += x * x;
    }
    return result;
};