/**
 * Created by chaika on 29.03.16.
 */
exports.CombinedTransform = function(a_transform, b_transform) {
    return function(bits) {
        var a = a_transform(bits);
        var b = b_transform(bits);

        return {
            encode: function(val) {
                return b.encode(a.encode(val));
            },
            decode: function(val) {
                return a.decode(b.decode(val));
            }
        }
    }
};