/**
 * Created by chaika on 29.03.16.
 */
//Варіант 2.
//Паралельна осям гіпереліпсоїдна функція, обтяжена сферична функція, зважена сферична модель (Axis parallel hyper-ellipsoid function, Weighted sphere model, extended homogeneous quadratic function):

module.exports = function(x_arr) {
    var result = 0;
    var size = x_arr.length;
    for(var i=0; i<size; ++i) {
        var x = x_arr[i];
        result += (i+1) * x * x;
    }
    return result;
};