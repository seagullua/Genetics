/**
 * Created by chaika on 29.03.16.
 */
var Pi2 = 2 * Math.PI;

module.exports = function(x_arr) {
    var size = x_arr.length;
    var result = 0;

    for(var i=0; i<size; ++i) {
        var x = x_arr[i];
        result += x*x;
    }
    result = Math.sqrt(result);
    return -1 * Math.cos(Pi2 * result) + 0.1 * result + 1;
};