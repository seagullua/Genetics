/**
 * Created by chaika on 29.03.16.
 */
var fitnessFunction = require('../utils/fitnessFunction');
var functions = require('../utils/functions');
var transforms = require('../utils/transforms');
var random = require('../utils/random');

var tr = transforms.Vector_RealToBinaryTransform(10, -5, 5);
var arr = random.generateRandomVectors(-5, 5, 5, 2);
arr.push([0,0,0,0,0]);
arr.push([0.1, -0.1, 0.1, 0.1, -0.2]);

arr = arr.map(tr.encode);

console.log("Start", arr);
var calculator = fitnessFunction.createFitnessCalculator(functions.Variant1, tr);

console.log("Iter1", arr.map(calculator));
console.log("Iter2", arr.map(calculator));

console.log(functions.Variant1([ 2.26]));