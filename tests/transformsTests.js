/**
 * Created by chaika on 29.03.16.
 */

var transforms = require('../utils/transforms');


var gray10 = transforms.Vector_RealToGrayTransform(10, -5.12, 5.12);
var binary10 = transforms.Vector_RealToBinaryTransform(10, -5.12, 5.12);
var none10 = transforms.Vector_NoneTransform();

function checkTransform(title, transform) {
    function show(val) {
        console.log(title, val, transform.encode(val));
    }
    function backAndForce(val) {
        var a = transform.encode(val);
        var b = transform.decode(a);
        console.log(title, val, b);
    }
    show([-5.12, 0, 3, 5.12]);
    show([0, 0]);
    backAndForce([-5.12, 0, 3, 5.12]);
    backAndForce([0, 0]);
}

checkTransform("Gray", gray10);
checkTransform("Binary", binary10);
checkTransform("None", none10);