/**
 * Created by chaika on 29.03.16.
 */
exports.VectorNoneTransform = function() {
    return {
        encode: function(arr) {
            return arr;
        },
        decode: function(arr) {
            return arr;
        },
        cacheKey: function() {
            //No key for real values
            return null;
        },
        cloneGenotype: function(genotype) {
            return genotype.slice(0);
        }
    }
};