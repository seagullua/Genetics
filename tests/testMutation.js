/**
 * Created by chaika on 29.03.16.
 */
var mutation = require('../utils/mutation');

function copy(a) {
    return JSON.parse(JSON.stringify(a));
}
function testMutation(start, mutation) {
    var small_prob = mutation(0.0000000000001);
    var big_prob = mutation(0.004);

    console.log(start);
    console.log(small_prob(copy(start)));
    //console.log(big_prob(copy(start)));
}


var bin = "0100110101011101110110111";
var real = [0, 1, 2, 3];

testMutation(bin, mutation.BinaryMutation());
testMutation(real, mutation.LinearRealMutation(0.1024));
testMutation(real, mutation.GaussRealMutation(0.6));

