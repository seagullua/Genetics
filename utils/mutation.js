/**
 * Created by chaika on 29.03.16.
 */
var binary = require('./mutation/binary');
var real = require('./mutation/real');

exports.BinaryMutation = binary.BinaryMutation;
exports.LinearRealMutation = real.LinearRealMutation;
exports.GaussRealMutation = real.GaussRealMutation;