/**
 * Created by chaika on 29.03.16.
 */
var random = require('../random');
var gaussian = require('gaussian');


function createRealMutation(probability, mut_fn) {
    return function(phenotype) {
        var mutated = false;
        for(var i=0; i<phenotype.length; ++i) {
            if(Math.random() < probability) {
                mutated = true;
                phenotype[i] = mut_fn(phenotype[i]);
            }
        }
        return {
            mutated: mutated,
            result: phenotype
        };
    }
}

function LinearRealMutation(r) {
    return function(probability) {
        return createRealMutation(probability, function(val){
            return val + random.randomNumber(-1, 1) * r;
        })
    }
}

function GaussRealMutation(v) {
    var distribution = gaussian(0, v);
    return function(probability) {
        return createRealMutation(probability, function(val){
            return val + distribution.ppf(Math.random());
        })
    }
}

exports.LinearRealMutation = LinearRealMutation;
exports.GaussRealMutation = GaussRealMutation;

