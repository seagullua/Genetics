/**
 * Created by chaika on 29.03.16.
 */
String.prototype.replaceAt=function(index, character) {
    return this.substr(0, index) + character + this.substr(index+character.length);
};

function getRandomInt(min, max_v) {
    var max = max_v - 1;
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var quickSort = require('../selection/sorts');

function QuickBitsMutation(probability) {
    var SIZE = 1000000;
    var cache_bits = [];
    var last_start = 0;


    function buildCache() {
        //console.log("Build cache", probability);
        var obj = {};
        var should_mutate = Math.round(probability * SIZE);
        for(var i = 0; i < should_mutate; ++i) {
            var id = getRandomInt(0, SIZE);
            obj[id] = id;
        }

        var keys = Object.keys(obj);
        var res = keys.map(function(key){
            return obj[key];
        });

        if(res.length > 1) {
            quickSort.sort(res);
        }
        cache_bits = res;
        last_start = 0;
    }

    buildCache();

    this.mutate = function(genotype) {
        var length = genotype.length;

        if(last_start + length >= SIZE) {
            buildCache();
        }

        var start = last_start;
        var end  = last_start + length;

        var mutate = [];
        while(cache_bits.length > 0 && cache_bits[0] < end) {
            mutate.push(cache_bits[0] - start);
            cache_bits.shift();
        }

        last_start = end;


        if(mutate.length == 0) {
            return {
                mutated: false,
                result: genotype
            }
        } else {
            var result = "";
            if(mutate.length == 1) {
                var pos = mutate[0];
                var ch = genotype[pos];
                if (ch == "0") {
                    result = genotype.replaceAt(pos, "1");
                } else {
                    result = genotype.replaceAt(pos, "0");
                }
            } else {
                var splitted = genotype.split("");
                for (var i = 0; i < mutate.length; ++i) {
                    var pos = mutate[i];
                    var ch = splitted[pos];
                    if (ch == "0") {
                        splitted[pos] = "1";
                    } else {
                        splitted[pos] = "0";
                    }
                }
                result = splitted.join("");
            }

            return {
                mutated: true,
                result: result
            }
        }
    }
};

exports.BinaryMutation = function() {
    return function(probability) {
        var mutator = new QuickBitsMutation(probability);
        return mutator.mutate;
    }
};