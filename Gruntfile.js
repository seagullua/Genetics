module.exports = function(grunt) {

    var Config = {
        browserify: {
            dist: {
                files: {
                    'output/gui.js': ['gui.js']
                }
            }
        },
        ejs: {
        }
        //watch: {
        //    //files: ['<%= jshint.files %>'],
        //    //tasks: ['jshint']
        //}
    };

    for(var i=1; i<=7; ++i) {
        Config.ejs["Variant"+i] = {
            options: {
                variant: "Variant" + i
            },
            src: 'report/gui.ejs',
            dest: 'output/gui_Variant'+i+'.html'
        }
    }

    grunt.initConfig(Config);



    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-ejs');

    grunt.registerTask('default', ['browserify', 'ejs']);

};