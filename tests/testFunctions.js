/**
 * Created by chaika on 29.03.16.
 */
var functions = require('../utils/functions');

function testFunction(title, fn) {
    function show(val) {
        console.log(title, val, fn(val));
    }
    show([0, 0, 0]);
    show([2, 2]);
    show([-5, 5]);
}

testFunction("Variant1", functions.Variant1);
testFunction("Variant2", functions.Variant2);