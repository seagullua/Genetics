/**
 * Created by chaika on 25.04.16.
 */
module.exports = function(x_arr) {
    var result = 0;
    var size = x_arr.length - 1;
    for(var i=0; i<size; ++i) {
        var x = x_arr[i];
        var nextX = x_arr[i+1];
        result += 100*(nextX - x*x)*(nextX - x*x) + (x -1)*(x -1);
    }
    return result;
};
