/**
 * Created by chaika on 29.03.16.
 */
var Environment = require('../utils/classes/Environment');
var fitnessFunction = require('../utils/fitnessFunction');
var functions = require('../utils/functions');
var transforms = require('../utils/transforms');
var random = require('../utils/random');
var selection = require('../utils/selection');
var mutation = require('../utils/mutation');

function runFunction(fn, bits_per_variable, variables, min, max) {
    var binaryMutation = mutation.BinaryMutation();
    var gaussMutation = mutation.GaussRealMutation(0.6);
    var linearMutation = mutation.LinearRealMutation(0.1024);
    var env = new Environment({
        //Transform: transforms.Vector_RealToGrayTransform(bits_per_variable, min, max),
        Transform: transforms.Vector_NoneTransform(),
        FitnessFunction: fn,
        Selection: selection.SUSSelectionMin(),
        PhenotypeGenerator: random.RealPhenotypeGenerator(min, max, variables),
        PopulationSize: 50,
        //Mutation: binaryMutation(0.00004),
        Mutation: linearMutation(0.001),
        MaxIterations: 500000,
        StatsSteps: 10000
    });

    console.log(env.runSimulation());

    //var starting_population = env.seedPopulation();
    //
    //for(var i=0; i<1000; ++i) {
    //    if(i % 150 == 0) {
    //        console.log("i", i, env.getPopulationFitness(starting_population));
    //        var zero_ind = starting_population.inidividuals[300];
    //        console.log(zero_ind.fitness, env.getIndividualPhenotype(zero_ind));
    //    }
    //
    //    starting_population = env.generateNewPopulation(starting_population);
    //
    //
    //}
}

setTimeout(function(){
    console.log("Start");
    runFunction(functions.Variant1, 10, 20, -5.12, 5.12);
    console.log("DONE");
}, 10);



