/**
 * Created by chaika on 30.03.16.
 */
var Tasks = require('../tasks/tasks');
var configuration = require('./configuration');
var OUTPUT_DIR = 'output/statistics';
var path = require('path');
var fse = require('fs-extra');
var Environment = require('./classes/Environment');
var RUNS_FOR_REPORT = 10;

var findPMax = require('./findPMax');

function generateReport(file, config, p_info) {
    var env = new Environment(config);
    var result = env.runSimulation();

    var REPORT = {
        Statistics: result,
        MaxIterations: config.MaxIterations,
        PopulationSize: config.PopulationSize,
        Params: p_info
    };

    writeJSON(file, REPORT);
}

function readJSON(file) {
    if(fse.existsSync(file)) {
        return fse.readJsonSync(file);
    }
    return null;
}

function writeJSON(file, json) {
    fse.writeJsonSync(file, json);
}


function generateStatisticsForConfig(dir, config, title, p_max) {
    var dir_name = path.join(OUTPUT_DIR, dir);

    fse.ensureDirSync(dir_name);

    var probabilities_to_check = Tasks.getProbabilitiesToCheck(p_max);
    probabilities_to_check.forEach(function(p_info){
        var p_config = configuration.cloneConfigForProbability(config, p_info.prob);

        for(var i=0; i<RUNS_FOR_REPORT; ++i) {
            var file_name = p_info.id+"_run"+i +".json";
            var REPORT_FILE = path.join(dir_name, file_name);

            if(!fse.existsSync(REPORT_FILE)) {
                console.log("Report", dir, file_name);
                generateReport(REPORT_FILE, p_config, p_info);
            }
        }
    });
}

function generateStatistics(fn_title) {
    var configs = configuration.generateConfigurations(Tasks.BaseTasks);
    var fn = Tasks.Functions[fn_title];

    if(!fn) {
        throw new Error("Wrong fn "+fn_title);
    }

    var dimensions = Tasks.FunctionVariables;

    configs = configuration.createFunctionAndDimensionsConfigs(configs, fn_title, fn, dimensions);

    var p_max = findPMax.findPMaxAllConfigs(fn_title);

    var results = {};
    Object.keys(configs).forEach(function(id){
        console.log("Generate Config ", id);
        generateStatisticsForConfig(id, configs[id], id, p_max[id]);
        results[id] = {
            config: configs[id],
            p_max: p_max[id]
        };
        console.log("Generate Config " + results[id], id);
    });
    return results;
}

function getReportsForId(id, params) {
    var dir = id;
    var config = params.config;
    var title = id;
    var p_max = params.p_max;
    var dir_name = path.join(OUTPUT_DIR, dir);

    fse.ensureDirSync(dir_name);

    var result = {};
    var probabilities_to_check = Tasks.getProbabilitiesToCheck(p_max);
    probabilities_to_check.forEach(function(p_info){
        var p_config = configuration.cloneConfigForProbability(config, p_info.prob);

        var reports = [];

        for(var i=0; i<RUNS_FOR_REPORT; ++i) {
            var file_name = p_info.id+"_run"+i +".json";
            var REPORT_FILE = path.join(dir_name, file_name);

            reports.push(readJSON(REPORT_FILE));
        }

        result[p_info.id] = {
            Statistics: reports,
            P_params: p_info,
            config: p_config
        }
    });

    return {
        Content: result,
        Probabilities: probabilities_to_check
    }
}

exports.getReportsForId = getReportsForId;
exports.writeJSON = writeJSON;

exports.generateStatistics = generateStatistics;