/**
 * Created by chaika on 30.03.16.
 */
function cloneConfig(config) {
    var new_conf = {};
    return append(new_conf, config);
}

function append(config, fields) {
    Object.keys(fields).forEach(function (key) {
        config[key] = fields[key];
    });
    return config;
}

function generateConfigurations(tasks) {
    var base_conf = cloneConfig(tasks.Common);
    base_conf.title = "";

    var pool = [base_conf];



    var vars = tasks.Variants;
    Object.keys(vars).forEach(function(id){
        var new_pool = [];
        var configs = vars[id];

        Object.keys(configs).forEach(function(var_title){
            pool.forEach(function(old_cfg){
                var cfg = cloneConfig(old_cfg);
                cfg = append(cfg, configs[var_title]);
                cfg.title += "_" + var_title;
                new_pool.push(cfg);
            });
        });

        pool = new_pool;
    });

    return pool;
}

exports.cloneConfigForVariables = function(config, variables) {
    var new_config = cloneConfig(config);
    new_config.PopulationSize = config.BasePopulationSize; //* variables;

    if(variables > 30) {
        new_config.PopulationSize /= 5;
        new_config.MaxIterations *= 5;
    }
    else if(variables >= 10) {
        if(variables > 10) {
            new_config.PopulationSize /= 2;
        }
        new_config.MaxIterations *= 2.5;
    }


    new_config.PhenotypeGenerator = new_config.PhenotypeGenerator(variables);
    //new_config.StatsSteps = Math.round(new_config.StatsSteps / variables / 100) * 100;
    return new_config;
};

exports.cloneConfigForProbability = function(config, probability) {
    var new_config = cloneConfig(config);
    new_config.Mutation = new_config.Mutation(probability);
    return new_config;
};

exports.cloneConfigForFunction = function(config, fitness_function) {
    var new_config = cloneConfig(config);
    new_config.FitnessFunction = fitness_function;
    return new_config;
};

exports.createFunctionAndDimensionsConfigs = function(configs, fn_title, fn, dimensions) {

    var base_dir = fn_title;



    configs = configs.map(function(cfg){
        return exports.cloneConfigForFunction(cfg, fn);
    });


    var results = {};

    dimensions.forEach(function(variables) {
        var local_path = base_dir + "/"  + variables;
        var local_configs = configs.map(function(cfg){
            return exports.cloneConfigForVariables(cfg, variables);
        });

        local_configs.forEach(function(cfg){
            var id = local_path + "/" +cfg.title;
            results[id] = cfg;
        });
    });
    return results;
};

exports.generateConfigurations = generateConfigurations;
exports.cloneConfig = cloneConfig;
exports.append = append;