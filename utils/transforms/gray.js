/**
 * Created by chaika on 29.03.16.
 */
var gray = require('gray-code');
var Cache = {};

function getGrayCache(bits) {
    if(!(bits in Cache)) {
        var codes = gray(bits);
        var encode = {};
        var decode = {};

        codes.forEach(function(code, i){
            var str = code.join("");
            encode[i] = str;
            decode[str] = i;
        });

        Cache[bits] = {
            encodeTransform: encode,
            decodeTransform: decode
        }
    }
    return Cache[bits];
}

function createTransform(transform) {
    return function(x) {
        if(!(x in transform)) {
            throw new Error("Can't transform "+ x);
        }
        return transform[x];
    }
}

function createGrayTransform(bits) {
    var transforms = getGrayCache(bits);
    return {
        encode: createTransform(transforms.encodeTransform),
        decode: createTransform(transforms.decodeTransform)
    }
}

exports.GrayTransform = function(){
    return createGrayTransform;
};