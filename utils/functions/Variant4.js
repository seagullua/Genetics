/**
 * Created by chaika on 29.03.16.
 */
var A = 10;
var w = 2 * Math.PI;

module.exports = function(x_arr) {
    var size = x_arr.length;
    var result = 10 * size;

    for(var i=0; i<size; ++i) {
        var x = x_arr[i];
        result += x*x - A * Math.cos(w * x);
    }
    return result;
};