/**
 * Created by chaika on 29.03.16.
 */
function createRealToIntegerTransform(bits, min, max) {
    min -= 0.00001;
    max += 0.00001;
    var intervals = Math.pow(2, bits);
    var total_length = (max - min);
    var interval_length = total_length / intervals;
    return {
        encode: function(x) {
            return Math.floor(((x - min) / interval_length));
        },
        decode: function(x) {
            return (x * interval_length) + min;
        }
    }
}

exports.RealToIntegerTransform = function(min, max) {
    return function(bits) {
        return createRealToIntegerTransform(bits, min, max);
    }
};