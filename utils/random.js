/**
 * Created by chaika on 29.03.16.
 */
var randomVector = require('./randomGenerators/randomVector');

function RealPhenotypeGenerator(min, max, chromosomes) {
    var generator = randomVector.RandomVectorGenerator(min, max);
    return function() {
        return generator(chromosomes);
    };
}

function RealPhenotypeGeneratorAnyLength(min, max) {
    return function(chromosomes) {
        return RealPhenotypeGenerator(min, max, chromosomes);
    }
}

function generatePhenotype(generator, quantity) {
    var result = [];
    for(var i=0; i<quantity; ++i) {
        result.push(generator());
    }
    return result;
}

function generateRandomVectors(min, max, N, quantity) {
    var generator = RealPhenotypeGenerator(min, max, N);
    return generatePhenotype(generator, quantity);
}

exports.RealPhenotypeGenerator = RealPhenotypeGenerator;
exports.RealPhenotypeGeneratorAnyLength = RealPhenotypeGeneratorAnyLength;
exports.generatePhenotype = generatePhenotype;

exports.generateRandomVectors = generateRandomVectors;
exports.randomNumber = randomVector.randomNumber;
exports.randomVector = randomVector.randomVector;