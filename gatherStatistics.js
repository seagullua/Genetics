/**
 * Created by chaika on 30.03.16.
 */
var Tasks = require('./tasks/tasks');
var gatherStatistics = require('./utils/gatherStatistics');

var variant = process.argv[2];
if(process.argv[3]) {
    Tasks.FunctionVariables = [parseInt(process.argv[3], 10)];
}
//

var result = gatherStatistics.generateStatistics(variant);
console.log(result);