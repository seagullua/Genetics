/**
 * Created by chaika on 31.03.16.
 */
var gatherStatistics = require('./gatherStatistics');

var LegendSample = {
    //Довжина ланцюжка
    l: null,
    //Розмір популяції
    N: null,
    //Метод кодування
    encoding: null,
    //Метод відбору
    selection: null,
    //Мутація
    mutation: null,
    //P_max
    p_max: null,
    //P_type (10*P_max)
    p_max_coef: null,
    //probability
    p: null
};

var RunColumns = {
    iterations: null,
    fitness_called: null,
    converged: null,

    //значення відхилення найкращого знайденого розв’язку від оптимального
    best_f_fopt: null,

    //Хемінгова відстань між найкращим знайденим та екстремумом
    best_opt_heming: null,
    //Евклідова відстань між найкращим знайденим та екстремумом
    best_opt_evklid: null,

    //значення відхилення середнього знайденого розв’язку від оптимального
    average_f_opt: null,

    errors_0: null,
    errors_1: null,
    errors_2: null,
    errors_3: null,
    errors_4: null,
    errors_5_more: null,

    distance_0: null,
    distance_1: null,
    distance_2: null,
    distance_3: null,
    distance_4: null,
    distance_5_more: null
};

var AverageColumn = {
    average_average: null,
    best_average: null,

    //Пристосованість найкращого
    best_f: null,
    //Найкращий фенотип
    best: null,

    average_iterations: null,
    average_converges: null
};

function cloneJson(json) {
    return JSON.parse(JSON.stringify(json));
}

function getAlgorithmParamsFromId(id) {
    var parts = id.split('/');
    var dimensions = parseInt(parts[1], 10);

    var params = parts[2].split("_");

    var SelectionParams = {
        SelectionSUS: "SUS",
        SelectionRWS: "RWS"
    };

    var LCoef = {
        EncodingRealLinear: 1,
        EncodingRealGauss: 1,
        EncodingBinaryBinary: 10,
        EncodingBinaryGray: 10
    };

    var Encoding = {
        EncodingRealLinear: "Real",
        EncodingRealGauss: "Real",
        EncodingBinaryBinary: "Binary",
        EncodingBinaryGray: "Gray"
    };

    var Mutation = {
        EncodingRealLinear: "Linear",
        EncodingRealGauss: "Gauss",
        EncodingBinaryBinary: "Bits",
        EncodingBinaryGray: "Bits"
    };

    return {
        l: dimensions * LCoef[params[1]],
        selection: SelectionParams[params[2]],
        encoding: Encoding[params[1]],
        mutation: Mutation[params[1]],
        variables: dimensions
    }
}

function diff(a,b){
    return Math.abs(a-b);
}

var evklidDistance = require('euclidean-distance');

function oneRunReport(data, COMPARE) {
    var result = cloneJson(RunColumns);
    var statistics = data.Statistics;
    result.iterations = statistics.iterations;
    result.fitness_called = statistics.fitnessCalled;
    result.converged = statistics.converged;

    result.best_f_fopt = diff(statistics.population.best_fitness, COMPARE.OptimalFitness);
    result.average_f_opt = diff(statistics.average, COMPARE.OptimalFitness);
    var best = statistics.population.best;

    result.best_opt_heming = COMPARE.HemmingDistance(best.genotype, COMPARE.OptimalGenotype);

    var best_phenotype = COMPARE.config.Transform.decode(best.genotype);
    result.best_opt_evklid = evklidDistance(best_phenotype, COMPARE.OptimalPhenotype);
    result.best_phenotype = best_phenotype;

    var errors = [0,0,0,0,0,0];
    var av_distance = [0,0,0,0,0,0];

    var inidividuals = statistics.population.inidividuals;
    inidividuals.forEach(function(ind){
        var genotype = ind.genotype;
        if(errors) {

            var distance = COMPARE.HemmingDistance(best.genotype, genotype);
            if(distance === null) {
                errors = null;
            } else {
                if(distance > 5) {
                    distance = 5;
                }
                errors[distance]++;
            }
        }

        var phenotype = COMPARE.config.Transform.decode(genotype);
        var dist = evklidDistance(phenotype, best_phenotype);

        dist = Math.round(dist * 100);
        if(dist > 5) {
            dist = 5;
        }

        av_distance[dist]++;

    });

    var N = inidividuals.length;

    if(errors) {
        result.errors_0 = errors[0] / N;
        result.errors_1 = errors[1] / N;
        result.errors_2 = errors[2] / N;
        result.errors_3 = errors[3] / N;
        result.errors_4 = errors[4] / N;
        result.errors_5_more = errors[5] / N;
    }


    result.distance_0 = av_distance[0] / N;
    result.distance_1 = av_distance[1] / N;
    result.distance_2 = av_distance[2] / N;
    result.distance_3 = av_distance[3] / N;
    result.distance_4 = av_distance[4] / N;
    result.distance_5_more = av_distance[5] / N;

    return result;
}

function getColumnAverage(columns) {
    var best_i = 0;
    var best_f = columns[0].best_f_fopt;

    var average_sum = 0;
    var best_sum = 0;
    var converges_sum = 0;
    var iterations_sum = 0;

    columns.forEach(function(c, i){
        if(c.best_f_fopt < best_f) {
            best_f = c.best_f_fopt;
            best_i = i;
        }

        average_sum += c.average_f_opt;
        best_sum += c.best_f_fopt;

        if(c.converged) {
            converges_sum++;
        }
        iterations_sum += c.iterations;
    });

    var result = cloneJson(AverageColumn);
    result.average_average = average_sum / columns.length;
    result.best_average = best_sum / columns.length;
    result.best_f = best_f;
    result.best = columns[best_i].best_phenotype;
    result.average_iterations = iterations_sum / columns.length;
    result.average_converges = converges_sum / columns.length;
    return result;
}

function buildOneReport(id, params) {
    var rows = [];

    var statistics = gatherStatistics.getReportsForId(id, params);

    var id_params = getAlgorithmParamsFromId(id);

    var base_legend = cloneJson(LegendSample);
    base_legend.l = id_params.l;
    base_legend.encoding = id_params.encoding;
    base_legend.selection = id_params.selection;
    base_legend.mutation = id_params.mutation;

    base_legend.N = params.config.PopulationSize;
    base_legend.p_max = params.p_max;

    var content = statistics.Content;

    var config = params.config;
    var COMPARE = {
        OptimalPhenotype: config.OptimalPhenotypeGenerator(id_params.variables)
    };
    COMPARE.OptimalGenotype = config.Transform.encode(COMPARE.OptimalPhenotype);
    COMPARE.OptimalFitness = config.FitnessFunction(COMPARE.OptimalPhenotype);
    COMPARE.HemmingDistance = config.HemmingDistance;
    COMPARE.config = config;

    //console.log(COMPARE, config);
    //throw "WW";

    Object.keys(content).forEach(function(key){
        var p_report = content[key];
        var legend = cloneJson(base_legend);

        legend.p_max_coef = p_report.P_params.title;
        legend.p = p_report.P_params.prob;

        var columns = p_report.Statistics.map(function(stats){
            return oneRunReport(stats, COMPARE);
        });

        var average = getColumnAverage(columns);


        rows.push({
            legend: legend,
            runs: columns,
            average: average
        });

    });
    return rows;
    //
    //console.log("Report Info", id, params);
    //console.log("params", base_legend);
}
var ejs = require('ejs');
var path = require('path');
var fs = require('fs');

function getPrecision(number) {
    if(number == 0) return 0;
    number = Math.abs(number);
    var precision = 0;
    while(Math.round(number) <= 0) {
        precision += 1;
        number *= 10;
    }
    return precision;
}

function formatProbability(value) {
    return formatNumber(value * 100) + "%";
}

function formatNumber(value) {
    var precision = getPrecision(value) + 1;
    return value.toFixed(precision)
}

function formatErrors(value) {
    if(value === null) {
        return null;
    }
    return Math.round(value * 100) + "%";
}

function saveReport(fn_title, results, suffix) {
    var dir = path.join(__dirname, '../report/report.ejs');
    var code = ejs.compile(fs.readFileSync(dir, "utf8"), {filename: dir});


    fs.writeFileSync('output/report_'+fn_title+suffix+".html", code({
        variant: fn_title,
        report: results,
        formatProbability: formatProbability,
        formatNumber: formatNumber,
        formatErrors: formatErrors,
        short: false,
        onePage: false,
        suffix: suffix
    }));

    fs.writeFileSync('output/report_'+fn_title+suffix+"_short.html", code({
        variant: fn_title,
        report: results,
        formatProbability: formatProbability,
        formatNumber: formatNumber,
        formatErrors: formatErrors,
        short: true,
        onePage: suffix != "",
        suffix: suffix
    }));
}

function buildReports(fn_title, suffix) {
    var configs = gatherStatistics.generateStatistics(fn_title);

    var results = {

    };
    console.log(Object.keys(configs));

    Object.keys(configs).forEach(function(id){
        var params =getAlgorithmParamsFromId(id);
        var vars = params.variables;
        if(!results[vars]) {
            results[vars] = [];
        }

        var rows = buildOneReport(id, configs[id]);
        results[vars] = results[vars].concat(rows);
    });

    //gatherStatistics.writeJSON("output/report.json", results);
    saveReport(fn_title, results, suffix);
}

var Tasks = require('../tasks/tasks');
exports.buildReports = function(fn_title){
    buildReports(fn_title, "");
    Tasks.getProbabilitiesToCheck = function(p_max) {
        return [{
            id: "P_max",
            title: "P_max",
            prob: p_max
        }]
    };
    buildReports(fn_title, "_pmax");
};