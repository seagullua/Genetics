/**
 * Created by chaika on 29.03.16.
 */
var random = require('../random');
var Population = require('../classes/Population');

var Mode = {
    Maximise: 0,
    Minimise: 1
};

function getModeProbabilityCalculator(mode, population) {
    if(mode == Mode.Maximise) {
        return function(p_rws) {
            return p_rws;
        }
    } else {
        var N = population.N;
        return function(p_rws) {
            return (1 - p_rws) / (N - 1);
        }
    }
}

function getIndividualProbabilityFunction(mode, environment, population) {
    var total = environment.getPopulationFitness(population);

    var mode_modifier = getModeProbabilityCalculator(mode, population);

    return function(individual) {
        var fitness = environment.getIndividualFitness(individual);
        var p_rws = fitness / total;
        return mode_modifier(p_rws);
    }
}

function generateNewPopulation(mode, environment, population) {
    var N = population.N;
    var points = random.randomVector(0, 1, N);
    return performRWSSelectionOnPoints(mode, environment, population, points);
}

function prepareTotalProbability(individuals, P_FN) {
    var total_p = 0;
    for(var i=0; i<individuals.length; ++i) {
        var ind = individuals[i];
        var p = P_FN(ind);
        ind._p_rws = total_p + p;
        total_p += p;
    }
    return total_p;
}

function iterateOverPoints(points, individuals) {
    var ind_i = 0;
    var prev_p = 0;
    var result = [];

    var N = individuals.length;

    for(var p_i=0; p_i < points.length; ++p_i) {
        var p_prob = points[p_i];

        while(ind_i < N) {
            var found = (prev_p <= p_prob && p_prob <= individuals[ind_i]._p_rws);
            if(found) {
                break;
            }
            prev_p = individuals[ind_i]._p_rws;
            ind_i++;
        }

        if(ind_i < N) {
            result.push(individuals[ind_i]);
        } else {
            result.push(individuals[N-1]);
        }
    }

    return result;
}

function clonePopulation(result, environment) {
    return new Population(Population.cloneIndividuals(result, environment));
}

var sorts = require('./sorts');
function performRWSSelectionOnPoints(mode, environment, population, points) {
    var individuals = population.inidividuals;
    var P_FN = getIndividualProbabilityFunction(mode, environment, population);

    prepareTotalProbability(individuals, P_FN);


    //console.log("RWS", individuals);
    sorts.sort(points);

    //return clonePopulation(population.inidividuals);

    var result = iterateOverPoints(points, individuals);

    return clonePopulation(result, environment);
}

function createRouletteWheelSelection(mode) {
    return function(environment) {
        return {
            generateNewPopulation: function(population) {
                return generateNewPopulation(mode, environment, population);
            }
        }
    }
}

exports.Mode = Mode;
exports.createRouletteWheelSelection = createRouletteWheelSelection;
exports.performRWSSelectionOnPoints = performRWSSelectionOnPoints;