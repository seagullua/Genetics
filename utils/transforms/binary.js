/**
 * Created by chaika on 29.03.16.
 */
var Cache = {};

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function getBinaryCache(bits) {
    if(!(bits in Cache)) {
        var last = Math.pow(2, bits);

        var encode = {};
        var decode = {};

        for(var i=0; i < last; ++i) {
            var str = new Number(i).toString(2);
            str = pad(str, bits);

            encode[i] = str;
            decode[str] = i;
        }
        Cache[bits] = {
            encodeTransform: encode,
            decodeTransform: decode
        }
    }
    return Cache[bits];
}

function createTransform(transform) {
    return function(x) {
        if(!(x in transform)) {
            throw new Error("Can't transform "+ x);
        }
        return transform[x];
    }
}

function createBinaryTransform(bits) {
    var transforms = getBinaryCache(bits);
    return {
        encode: createTransform(transforms.encodeTransform),
        decode: createTransform(transforms.decodeTransform)
    }
}

exports.BinaryTransform = function(){
    return createBinaryTransform;
};